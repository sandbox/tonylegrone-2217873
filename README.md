Commerce Status Access
======================
Provides new permissions used to provide update access to users on specific
orders without giving them full administration permission.

The inspiring use case for this module is a fulfillment role that can change an
order from *Pending* to *Processing* to *Completed*, but only when an order is
passed the cart/checkout statuses.
